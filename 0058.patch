From c8bc80643d4a6c0ef27c48e680907d6fd3c888cc Mon Sep 17 00:00:00 2001
From: Jan Macku <jamacku@redhat.com>
Date: Mon, 12 Feb 2024 16:58:16 +0100
Subject: [PATCH] ci: enable source-git automation

- commit validation
- pull request validation
- tracker validation
- automatic merging

rhel-only

Related: RHEL-30581
---
 .github/advanced-commit-linter.yml                 | 17 ++++++
 .github/auto-merge.yml                             |  4 ++
 .github/pull-request-validator.yml                 |  4 ++
 .github/tracker-validator.yml                      | 28 +++++++++
 .github/workflows/gather-metadata.yml              | 28 +++++++++
 .../workflows/source-git-automation-on-demand.yml  | 70 ++++++++++++++++++++++
 .github/workflows/source-git-automation.yml        | 46 ++++++++++++++
 7 files changed, 197 insertions(+)

diff --git a/.github/advanced-commit-linter.yml b/.github/advanced-commit-linter.yml
new file mode 100644
index 00000000..0180ef07
--- /dev/null
+++ b/.github/advanced-commit-linter.yml
@@ -0,0 +1,17 @@
+policy:
+  cherry-pick:
+    upstream:
+      - github: dracutdevs/dracut
+    exception:
+      note:
+        - rhel-only
+        - RHEL-only
+  tracker:
+    - keyword:
+        - 'Resolves: '
+        - 'Related: '
+        - 'Reverts: '
+      type: jira
+      issue-format:
+        - 'RHEL-\d+$'
+      url: 'https://issues.redhat.com/browse/'
diff --git a/.github/auto-merge.yml b/.github/auto-merge.yml
new file mode 100644
index 00000000..35c25392
--- /dev/null
+++ b/.github/auto-merge.yml
@@ -0,0 +1,4 @@
+labels:
+  dont-merge: dont-merge
+  manual-merge: pr/needs-manual-merge
+target-branch': ['main']
diff --git a/.github/pull-request-validator.yml b/.github/pull-request-validator.yml
new file mode 100644
index 00000000..4bb5bbec
--- /dev/null
+++ b/.github/pull-request-validator.yml
@@ -0,0 +1,4 @@
+labels:
+  missing-review: pr/needs-review
+  changes-requested: pr/changes-requested
+  missing-failing-ci: pr/needs-ci
diff --git a/.github/tracker-validator.yml b/.github/tracker-validator.yml
new file mode 100644
index 00000000..f88cc0a5
--- /dev/null
+++ b/.github/tracker-validator.yml
@@ -0,0 +1,28 @@
+labels:
+  missing-tracker: tracker/missing
+  invalid-product: tracker/invalid-product
+  invalid-component: tracker/invalid-component
+  unapproved: tracker/unapproved
+products:
+  - Red Hat Enterprise Linux 9
+  - CentOS Stream 9
+  - rhel-9.0.0
+  - rhel-9.0.0.z
+  - rhel-9.2.0
+  - rhel-9.2.0.z
+  - rhel-9.3.0
+  - rhel-9.3.0.z
+  - rhel-9.4.0
+  - rhel-9.4.0.z
+  - rhel-9.5.0
+  - rhel-9.5.0.z
+  - rhel-9.6.0
+  - rhel-9.6.0.z
+  - rhel-9.7.0
+  - rhel-9.7.0.z
+  - rhel-9.8.0
+  - rhel-9.8.0.z
+  - rhel-9.9.0
+  - rhel-9.9.0.z
+  - rhel-9.10.0
+  - rhel-9.10.0.z
diff --git a/.github/workflows/gather-metadata.yml b/.github/workflows/gather-metadata.yml
new file mode 100644
index 00000000..e4cbc486
--- /dev/null
+++ b/.github/workflows/gather-metadata.yml
@@ -0,0 +1,28 @@
+name: Gather Pull Request Metadata
+on:
+  pull_request:
+    types: [ opened, reopened, synchronize ]
+    branches:
+      - main
+      - rhel-9.*
+
+permissions:
+  contents: read
+
+jobs:
+  gather-metadata:
+    runs-on: ubuntu-latest
+
+    steps:
+      - name: Repository checkout
+        uses: actions/checkout@v4
+
+      - id: Metadata
+        name: Gather Pull Request Metadata
+        uses: redhat-plumbers-in-action/gather-pull-request-metadata@v1
+
+      - name: Upload artifact with gathered metadata
+        uses: actions/upload-artifact@v3
+        with:
+          name: pr-metadata
+          path: ${{ steps.Metadata.outputs.metadata-file }}
diff --git a/.github/workflows/source-git-automation-on-demand.yml b/.github/workflows/source-git-automation-on-demand.yml
new file mode 100644
index 00000000..8a3a366e
--- /dev/null
+++ b/.github/workflows/source-git-automation-on-demand.yml
@@ -0,0 +1,70 @@
+name: Source git Automation Scheduled/On Demand
+on:
+  schedule:
+    # Workflow runs every 45 minutes
+    - cron: '*/45 * * * *'
+  workflow_dispatch:
+    inputs:
+      pr-number:
+        description: 'Pull Request number/s ; when not provided, the workflow will run for all open PRs'
+        required: true
+        default: '0'
+
+permissions:
+  contents: read
+
+jobs:
+  # Get all open PRs
+  gather-pull-requests:
+    if: github.repository == 'redhat-plumbers/dracut-rhel9'
+    runs-on: ubuntu-latest
+
+    outputs:
+      pr-numbers: ${{ steps.get-pr-numbers.outputs.result }}
+      pr-numbers-manual: ${{ steps.parse-manual-input.outputs.result }}
+
+    steps:
+      - id: get-pr-numbers
+        if: inputs.pr-number == '0'
+        name: Get all open PRs
+        uses: actions/github-script@v6
+        with:
+          # !FIXME: this is not working if there is more than 100 PRs opened
+          script: |
+            const { data: pullRequests } = await github.rest.pulls.list({
+              owner: context.repo.owner,
+              repo: context.repo.repo,
+              state: 'open',
+              per_page: 100
+            });
+            return pullRequests.map(pr => pr.number);
+      - id: parse-manual-input
+        if: inputs.pr-number != '0'
+        name: Parse manual input
+        run: |
+          echo "result="[ ${{ inputs.pr-number }} ]"" >> $GITHUB_OUTPUT
+        shell: bash
+
+  validate-pr:
+    name: 'Validation of Pull Request #${{ matrix.pr-number }}'
+    needs: [ gather-pull-requests ]
+    runs-on: ubuntu-latest
+
+    strategy:
+      fail-fast: false
+      matrix:
+        pr-number: ${{ inputs.pr-number == 0 && fromJSON(needs.gather-pull-requests.outputs.pr-numbers) || fromJSON(needs.gather-pull-requests.outputs.pr-numbers-manual) }}
+
+    permissions:
+      # required for merging PRs
+      contents: write
+      # required for PR comments and setting labels
+      pull-requests: write
+
+    steps:
+      - name: Source-git Automation
+        uses: redhat-plumbers-in-action/source-git-automation@v1
+        with:
+          pr-number: ${{ matrix.pr-number }}
+          jira-api-token: ${{ secrets.JIRA_API_TOKEN }}
+          token: ${{ secrets.GITHUB_TOKEN }}
diff --git a/.github/workflows/source-git-automation.yml b/.github/workflows/source-git-automation.yml
new file mode 100644
index 00000000..025ee84d
--- /dev/null
+++ b/.github/workflows/source-git-automation.yml
@@ -0,0 +1,46 @@
+name: Source git Automation
+on:
+  workflow_run:
+    workflows: [ Gather Pull Request Metadata ]
+    types:
+      - completed
+
+permissions:
+  contents: read
+
+jobs:
+  download-metadata:
+    if: >
+      github.event.workflow_run.event == 'pull_request' &&
+      github.event.workflow_run.conclusion == 'success' &&
+      github.repository == 'redhat-plumbers/dracut-rhel9'
+    runs-on: ubuntu-latest
+
+    outputs:
+      pr-metadata: ${{ steps.Artifact.outputs.pr-metadata-json }}
+
+    steps:
+      - id: Artifact
+        name: Download Artifact
+        uses: redhat-plumbers-in-action/download-artifact@v1
+        with:
+          name: pr-metadata
+
+  source-git-automation:
+    needs: [ download-metadata ]
+    runs-on: ubuntu-latest
+
+    permissions:
+      # required for merging PRs
+      contents: write
+      # required for PR comments and setting labels
+      pull-requests: write
+
+
+    steps:
+      - name: Source-git Automation
+        uses: redhat-plumbers-in-action/source-git-automation@v1
+        with:
+          pr-metadata: ${{ needs.download-metadata.outputs.pr-metadata }}
+          jira-api-token: ${{ secrets.JIRA_API_TOKEN }}
+          token: ${{ secrets.GITHUB_TOKEN }}

